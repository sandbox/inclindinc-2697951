<?php

class MSDynamicsClient {
  protected $service_url, $company_id;

  public function __construct() {
    $this->service_url = variable_get('commerce_dynamics_service_url', NULL);
    $this->company_id = variable_get('commerce_dynamics_company_id', NULL);
  }

  public function request($method, array $arguments = array()) {
    try {
      // send request
      $uri = $this->service_url . $method;
      return drupal_http_request($uri)->data;
      // return response
    } catch (Exception $error) {
      // log error
      watchdog('msdynamics_client', 'Something happened - @error', array('@error' => $error->getMessage()), WATCHDOG_CRITICAL);
      return FALSE;
    }
  }
}
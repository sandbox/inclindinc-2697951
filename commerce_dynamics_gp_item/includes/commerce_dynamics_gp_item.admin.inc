<?php

/**
 * Form builder for the user alerts settings form.
 * @ingroup forms
 */
function commerce_dynamics_gp_item_admin_settings() {
  $form = array();

  $form['commerce_dynamics_gp_item_local_count'] = array(
    '#title' => '',
    '#type' => 'item',
    '#markup' => t('<h3><strong>Local:</strong> 26 items</h3> <h3><strong>Dynamics Great Plains:</strong>27 items</h3>'),
  );

  return system_settings_form($form);
}
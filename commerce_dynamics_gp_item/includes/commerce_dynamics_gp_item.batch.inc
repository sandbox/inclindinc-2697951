<?php

function commerce_dynamics_gp_item_batch_import_form() {
  // a form that has a 'sync' button to start a sync of products to GP
}

function commerce_dynamics_gp_item_batch_import_form_submit() {
  // create the batch
  // call the batch
}

function commerce_dynamics_gp_item_batch_gp_sync() {
  // invoke the batch
  // request items only last_modified > variable_get('commerce_dynamics_gp_item_last_sync', REQUEST_TIME)
  // method items/get/filter/{timestamp}
}

function commerce_dynamics_gp_item_batch_gp_process_item() {
  // process the item
  // load product with commerce_product_load_by_sku
  // if false $product = commerce_product_create_product (lookup actual function)
  // set properties, name, price, sku, title, quantity
  // commerce_product_save() the $product object
}

function commerce_dynamics_gp_item_batch_gp_finished() {
  // the batch, once finished
  // report count of items processed
}
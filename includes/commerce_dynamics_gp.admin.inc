<?php

/**
 * Commerce Dynamics admin settings form.
 * @param $form
 * @param $form_state
 * @return mixed
 */
function commerce_dynamics_gp_settings($form, $form_state) {
  $form['commerce_dynamics_gp_company_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Company ID'),
    '#default_value' => variable_get('commerce_dynamics_gp_company_id', NULL),
    '#description' => 'The Company ID (key) in Microsoft Dynamics that will be queried.',
  );

  $form['commerce_dynamics_gp_service_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Base Service URL'),
    '#default_value' => variable_get('commerce_dynamics_gp_service_url', NULL),
    '#description' => t('The base URL of the web service in Microsoft Dynamics (ex. @service)', array('@service' => 'http://<name>:<port>/Service1.svc/')),
  );

  return system_settings_form($form);
}
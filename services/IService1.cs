﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web.Services;
using GPWCFServiceTest.GPDrupalService;

namespace GPWCFServiceTest
{
    [ServiceContract]
    public interface IInventory
    {
        // Retrieve a list of items from Dynamics.
        // The response is cached.
        [OperationContract]
        [WebGet(UriTemplate = "item/get/all", BodyStyle = WebMessageBodyStyle.WrappedResponse, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [AspNetCacheProfile("CacheFor300Seconds")]
        ItemList[] GetItemList();

        // Retrieve an item by its key from Dynamics
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "item/get/{id}", BodyStyle = WebMessageBodyStyle.WrappedResponse, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Item GetItemByKey(string id);

        // Retrieve an item by its key from Dynamics
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "item/get/{id}/price", BodyStyle = WebMessageBodyStyle.WrappedResponse, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Pricing GetPricingByKey(string id);

        // TODO: MOVE THE REST INTO THEIR OWN WCF SERVICES
        // EACH NEEDS TO BE AN INTERFACE, SERVICE, AND CONTRACT LIKE IInventory

        // ORDER DOC CRUD
        // Create a Sales document from an order.
        /*[OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "document/create", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        SalesDocument CreateSalesDocument(object document);

        // Get a Sales document.
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "document/update/{id}", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        SalesDocument GetSalesDocument(string id, object document);

        // Update a Sales document from an order.
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "document/update/{id}", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        SalesDocument UpdateSalesDocument(string id, object document);*/

        // CUSTOMER CRUD
        // Create a Customer.
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "customer/create", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void CreateCustomer(Customer customer);

        // Get a Customer.
        [OperationContract]
        [WebGet(UriTemplate = "customer/get/{id}", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [AspNetCacheProfile("CacheFor300Seconds")]
        Customer GetCustomerByKey(string id);

        // Update a Customer.
        /*[OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "customer/update/{id}", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Customer UpdateCustomer(string id, Customer customer);*/
    }

    // Holds a list of summary objects for items.
    [DataContract]
    public class ItemList
    {
        [DataMember]
        public string id { get; set; }

        [DataMember]
        public string description { get; set; }

        [DataMember]
        public long created { get; set; }

        [DataMember]
        public long last_modified { get; set; }
    }
}

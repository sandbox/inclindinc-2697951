﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using GPWCFServiceTest.GPDrupalService;

namespace GPWCFServiceTest
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class InventoryService : IInventory
    {

        [AspNetCacheProfile("CacheFor300Seconds")]
        public ItemList[] GetItemList()
        {
            OutgoingWebResponseContext outResponse = WebOperationContext.Current.OutgoingResponse;
            IncomingWebRequestContext inRequest = WebOperationContext.Current.IncomingRequest;

            CompanyKey companyKey;
            Context context;
            LikeRestrictionOfstring itemClassIdRestriction;
            ItemCriteria itemCriteria;
            ItemSummary[] itemSummaries;
            List<ItemList> items = new List<ItemList>();
            
            // Create an instance of the service
            DynamicsGPClient wsDynamicsGP = new DynamicsGPClient();

            // Pass authentication
            // @todo: replace with a generic user
            wsDynamicsGP.ClientCredentials.Windows.ClientCredential.UserName = "Administrator";
            wsDynamicsGP.ClientCredentials.Windows.ClientCredential.Password = "TesT1234";

            // Create a context with which to call the service
            context = new Context();

            // Specify which company to use (sample company)
            companyKey = new CompanyKey();
            companyKey.Id = (-1);

            // Set up the context object
            context.OrganizationKey = (OrganizationKey)companyKey;

            // Create a restriction object
            // Retrieve all items in the CATALOG class
            itemClassIdRestriction = new LikeRestrictionOfstring();
            itemClassIdRestriction.EqualValue = "CATALOG";

            // Create a item criteria object
            itemCriteria = new ItemCriteria();
            itemCriteria.ItemClassId = itemClassIdRestriction;

            // Retrieve the list of item summary objects
            itemSummaries = wsDynamicsGP.GetItemList(itemCriteria, context);
             
            foreach (ItemSummary summary in itemSummaries)
            {
                items.Add(new ItemList
                    {
                        id = summary.Key.Id,
                        description = summary.Description,
                        last_modified = convertToTimestamp(summary.LastModifiedDate.Value.ToUniversalTime())
                    }
                );
            }
            
            // Close the service
            if (wsDynamicsGP.State != CommunicationState.Faulted)
            {
                wsDynamicsGP.Close();
            }

            DateTime expires = DateTime.Now.AddHours(1);
            outResponse.Headers.Add("Expires", expires.ToString("ddd, dd MMM yyyy H:mm:s K"));

            return items.ToArray();
        }

        public Item GetItemByKey(string id)
        {
            CompanyKey companyKey;
            Context context;
            ItemKey itemKey;
            Item item;

            // Create an instance of the service
            DynamicsGPClient wsDynamicsGP = new DynamicsGPClient();

            // Pass authentication
            // @todo: replace with a generic user
            wsDynamicsGP.ClientCredentials.Windows.ClientCredential.UserName = "Administrator";
            wsDynamicsGP.ClientCredentials.Windows.ClientCredential.Password = "TesT1234";

            // Create a context with which to call the service
            context = new Context();

            // Specify which company to use (sample company)
            companyKey = new CompanyKey();
            companyKey.Id = (-1);

            // Set up the context object
            context.OrganizationKey = (OrganizationKey)companyKey;

            // Create an item key object to specify the item
            itemKey = new ItemKey();
            itemKey.Id = id;

            // Retrieve the item object
            item = wsDynamicsGP.GetItemByKey(itemKey, context);

            // Close the service
            if (wsDynamicsGP.State != CommunicationState.Faulted)
            {
                wsDynamicsGP.Close();
            }

            return item;
        }

        public Pricing GetPricingByKey(string id)
        {
            CompanyKey companyKey;
            Context context;
            ItemKey itemKey;
            PriceLevelKey priceLevelKey;
            PricingKey pricingKey;
            Pricing pricing;

            // Create an instance of the service
            DynamicsGPClient wsDynamicsGP = new DynamicsGPClient();

            // Pass authentication
            // @todo: replace with a generic user
            wsDynamicsGP.ClientCredentials.Windows.ClientCredential.UserName = "Administrator";
            wsDynamicsGP.ClientCredentials.Windows.ClientCredential.Password = "TesT1234";

            // Create a context with which to call the service
            context = new Context();

            // Specify which company to use (sample company)
            companyKey = new CompanyKey();
            companyKey.Id = (-1);

            // Set up the context object
            context.OrganizationKey = (OrganizationKey)companyKey;

            // Create an item key to specify the item
            itemKey = new ItemKey();
            itemKey.Id = id;

            // Create a price level key to specify the price level
            priceLevelKey = new PriceLevelKey();
            priceLevelKey.Id = "RETAIL";

            // Create a currency key to specify the price level
            CurrencyKey currencyKey = new CurrencyKey();
            currencyKey.ISOCode = "USD";

            // Create a pricing key to specify the pricing object
            pricingKey = new PricingKey();
            pricingKey.ItemKey = itemKey;
            pricingKey.PriceLevelKey = priceLevelKey;
            pricingKey.CurrencyKey = currencyKey;
            pricingKey.UofM = "Each";

            // Get the specified pricing object
            pricing = wsDynamicsGP.GetPricingByKey(pricingKey, context);

            // Close the service
            if (wsDynamicsGP.State != CommunicationState.Faulted)
            {
                wsDynamicsGP.Close();
            }

            return pricing;
        }

        public Customer GetCustomerByKey(string id)
        {
            CompanyKey companyKey;
            Context context;
            Customer customer;
            CustomerKey customerKey;

            // Create an instance of the service
            DynamicsGPClient wsDynamicsGP = new DynamicsGPClient();
            wsDynamicsGP.ClientCredentials.Windows.ClientCredential.UserName = "Administrator";
            wsDynamicsGP.ClientCredentials.Windows.ClientCredential.Password = "TesT1234";

            // Create a context with which to call the service
            context = new Context();

            // Specify which company to use (sample company)
            companyKey = new CompanyKey();
            companyKey.Id = (-1);

            // Set up the context
            context.OrganizationKey = (OrganizationKey)companyKey;

            // Create a customer key
            customerKey = new CustomerKey();
            customerKey.Id = id;

            // Retrieve the customer object
            customer = wsDynamicsGP.GetCustomerByKey(customerKey, context);

            // Close the service
            if (wsDynamicsGP.State != CommunicationState.Faulted)
            {
                wsDynamicsGP.Close();
            }

            return customer;
        }

        public void CreateCustomer(Customer customer)
        {
            CompanyKey companyKey;
            Context context;
            //Customer customer;
            CustomerKey customerKey;
            Policy customerPolicy;

            // Create an instance of the service
            DynamicsGPClient wsDynamicsGP = new DynamicsGPClient();
            wsDynamicsGP.ClientCredentials.Windows.ClientCredential.UserName = "Administrator";
            wsDynamicsGP.ClientCredentials.Windows.ClientCredential.Password = "TesT1234";

            // Create a context with which to call the service
            context = new Context();

            // Specify which company to use (sample company)
            companyKey = new CompanyKey();
            companyKey.Id = (-1);

            // Set up the context
            context.OrganizationKey = (OrganizationKey)companyKey;

            // Create a new customer object
            customer = new Customer();

            // Create a customer key
            customerKey = new CustomerKey();
            customerKey.Id = "23743264";
            customer.Key = customerKey;

            // Set properties for the new customer
            //customer.Name = requestData.Name;

            // Get the create policy for the customer
            customerPolicy = wsDynamicsGP.GetPolicyByOperation("CreateCustomer", context);

            // Create the customer
            wsDynamicsGP.CreateCustomer(customer, context, customerPolicy);

            // Close the service
            if (wsDynamicsGP.State != CommunicationState.Faulted)
            {
                wsDynamicsGP.Close();
            }
        }

        private static readonly DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        private static long convertToTimestamp(DateTime value)
        {
            TimeSpan elapsedTime = value - Epoch;
            return (long)elapsedTime.TotalSeconds;
        }
    }
}
